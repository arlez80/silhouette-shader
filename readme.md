# Silhouette Shader

## How to Use

### Silhouette

add ShaderMaterial with silhouette.shader to next pass.

### Outline Silhouette

+ add ShaderMaterial with silhouette_outline_stencil.shader to next pass.
+ set 126 to render priority
+ add ShaderMaterial with silhouette_outline.shader to silhouette_outline_stencil next pass.
+ set 127 to render priority

## License

MIT License

## Author

あるる（きのもと 結衣）/ Yui Kinomoto @arlez80
